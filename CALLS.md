## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Sonatype Nexus Repository. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Sonatype Nexus Repository.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Sonatype Nexus Repository. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getUserSources(callback)</td>
    <td style="padding:15px">Retrieve a list of the available user sources.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/user-sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(userId, body, callback)</td>
    <td style="padding:15px">Update an existing user.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(userId, callback)</td>
    <td style="padding:15px">Delete a user.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(userId, source, callback)</td>
    <td style="padding:15px">Retrieve a list of users. Note if the source is not 'default' the response is limited to 100 users.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create a new user in the default source.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(userId, body, callback)</td>
    <td style="padding:15px">Change a user's password.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/users/{pathv1}/change-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrivileges(callback)</td>
    <td style="padding:15px">Retrieve a list of privileges.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrivilege(privilegeId, callback)</td>
    <td style="padding:15px">Retrieve a privilege by id.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePrivilege(privilegeId, callback)</td>
    <td style="padding:15px">Delete a privilege by id.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrivilege(body, callback)</td>
    <td style="padding:15px">Create a wildcard type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/wildcard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrivilege1(body, callback)</td>
    <td style="padding:15px">Create an application type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivilege(privilegeId, body, callback)</td>
    <td style="padding:15px">Update a wildcard type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/wildcard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivilege1(privilegeId, body, callback)</td>
    <td style="padding:15px">Update an application type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/application/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrivilege2(body, callback)</td>
    <td style="padding:15px">Create a repository content selector type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/repository-content-selector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrivilege3(body, callback)</td>
    <td style="padding:15px">Create a repository admin type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/repository-admin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrivilege4(body, callback)</td>
    <td style="padding:15px">Create a repository view type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/repository-view?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivilege2(privilegeId, body, callback)</td>
    <td style="padding:15px">Update a repository view type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/repository-view/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivilege3(privilegeId, body, callback)</td>
    <td style="padding:15px">Update a repository content selector type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/repository-content-selector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivilege4(privilegeId, body, callback)</td>
    <td style="padding:15px">Update a repository admin type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/repository-admin/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrivilege5(body, callback)</td>
    <td style="padding:15px">Create a script type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/script?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivilege5(privilegeId, body, callback)</td>
    <td style="padding:15px">Update a script type privilege.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/privileges/script/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealms(callback)</td>
    <td style="padding:15px">List the available realms</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/realms/available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveRealms(callback)</td>
    <td style="padding:15px">List the active realm IDs in order</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/realms/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setActiveRealms(body, callback)</td>
    <td style="padding:15px">Set the active security realms in the order they should be used</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/realms/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(source, callback)</td>
    <td style="padding:15px">List roles</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(body, callback)</td>
    <td style="padding:15px">Create role</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRole(source, id, callback)</td>
    <td style="padding:15px">Get role</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(id, body, callback)</td>
    <td style="padding:15px">Update role</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(id, callback)</td>
    <td style="padding:15px">Delete role</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskById(id, callback)</td>
    <td style="padding:15px">Get a single task by id</td>
    <td style="padding:15px">{base_path}/{version}/v1/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasks(type, callback)</td>
    <td style="padding:15px">List tasks</td>
    <td style="padding:15px">{base_path}/{version}/v1/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">run(id, callback)</td>
    <td style="padding:15px">Run task</td>
    <td style="padding:15px">{base_path}/{version}/v1/tasks/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stop(id, callback)</td>
    <td style="padding:15px">Stop task</td>
    <td style="padding:15px">{base_path}/{version}/v1/tasks/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBlobStore(name, callback)</td>
    <td style="padding:15px">Delete a blob store by name</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBlobStores(callback)</td>
    <td style="padding:15px">List the blob stores</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">quotaStatus(id, callback)</td>
    <td style="padding:15px">Get quota status for a given blob store</td>
    <td style="padding:15px">{base_path}/{version}/v1/blobstores/{pathv1}/quota-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFileBlobStore(body, callback)</td>
    <td style="padding:15px">Create a file blob store</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileBlobStoreConfiguration(name, callback)</td>
    <td style="padding:15px">Get a file blob store configuration by name</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFileBlobStore(name, body, callback)</td>
    <td style="padding:15px">Update a file blob store configuration by name</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBlobStore(name, callback)</td>
    <td style="padding:15px">Fetch a S3 blob store configuration</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores/s3/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBlobStore(body, name, callback)</td>
    <td style="padding:15px">Update an S3 blob store configuration</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores/s3/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBlobStore(body, callback)</td>
    <td style="padding:15px">Create an S3 blob store</td>
    <td style="padding:15px">{base_path}/{version}/beta/blobstores/s3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhase(callback)</td>
    <td style="padding:15px">Get current lifecycle phase</td>
    <td style="padding:15px">{base_path}/{version}/v1/lifecycle/phase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPhase(body, callback)</td>
    <td style="padding:15px">Move to new lifecycle phase</td>
    <td style="padding:15px">{base_path}/{version}/v1/lifecycle/phase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bounce(body, callback)</td>
    <td style="padding:15px">Bounce lifecycle phase</td>
    <td style="padding:15px">{base_path}/{version}/v1/lifecycle/bounce?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">freeze(callback)</td>
    <td style="padding:15px">Enable read-only</td>
    <td style="padding:15px">{base_path}/{version}/v1/read-only/freeze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceRelease(callback)</td>
    <td style="padding:15px">Forcibly release read-only</td>
    <td style="padding:15px">{base_path}/{version}/v1/read-only/force-release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">release(callback)</td>
    <td style="padding:15px">Release read-only</td>
    <td style="padding:15px">{base_path}/{version}/v1/read-only/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(callback)</td>
    <td style="padding:15px">Get read-only state</td>
    <td style="padding:15px">{base_path}/{version}/v1/read-only?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeCertificate(id, callback)</td>
    <td style="padding:15px">Remove a certificate in the trust store.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ssl/truststore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustStoreCertificates(callback)</td>
    <td style="padding:15px">Retrieve a list of certificates added to the trust store.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ssl/truststore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCertificate(body, callback)</td>
    <td style="padding:15px">Add a certificate to the trust store.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ssl/truststore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveCertificate(host, port, protocolHint, callback)</td>
    <td style="padding:15px">Helper method to retrieve certificate details from a remote system.</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ssl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositories(callback)</td>
    <td style="padding:15px">List repositories</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invalidateCache(repositoryName, callback)</td>
    <td style="padding:15px">Invalidate repository cache. Proxy or group repositories only.</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/{pathv1}/invalidate-cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepository(repositoryName, callback)</td>
    <td style="padding:15px">Delete repository of any format</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebuildIndex(repositoryName, callback)</td>
    <td style="padding:15px">Schedule a 'Repair - Rebuild repository search' Task. Hosted or proxy repositories only.</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/{pathv1}/rebuild-index?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository(body, callback)</td>
    <td style="padding:15px">Create Maven hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/maven/hosted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Maven hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/maven/hosted/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository1(body, callback)</td>
    <td style="padding:15px">Create Maven proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/maven/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository1(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Maven proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/maven/proxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableRepositoryHealthCheck(repositoryName, callback)</td>
    <td style="padding:15px">Enable Repository Health Check. Proxy repositories only.</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/{pathv1}/health-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableRepositoryHealthCheck(repositoryName, callback)</td>
    <td style="padding:15px">Disable Repository Health Check. Proxy repositories only.</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/{pathv1}/health-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository2(body, callback)</td>
    <td style="padding:15px">Create APT hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/apt/hosted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository2(body, repositoryName, callback)</td>
    <td style="padding:15px">Update APT hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/apt/hosted/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository3(body, callback)</td>
    <td style="padding:15px">Create APT proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/apt/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository3(body, repositoryName, callback)</td>
    <td style="padding:15px">Update APT proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/apt/proxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository4(body, callback)</td>
    <td style="padding:15px">Create a Go group repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/go/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository4(body, repositoryName, callback)</td>
    <td style="padding:15px">Update a Go group repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/go/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository5(body, callback)</td>
    <td style="padding:15px">Create a Go proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/go/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository5(body, repositoryName, callback)</td>
    <td style="padding:15px">Update a Go proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/go/proxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository6(body, callback)</td>
    <td style="padding:15px">Create Helm hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/helm/hosted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository6(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Helm hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/helm/hosted/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository7(body, callback)</td>
    <td style="padding:15px">Create Helm proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/helm/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository7(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Helm proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/helm/proxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository8(body, callback)</td>
    <td style="padding:15px">Create P2 proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/p2/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository8(body, repositoryName, callback)</td>
    <td style="padding:15px">Update P2 proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/p2/proxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository9(body, callback)</td>
    <td style="padding:15px">Create Bower group repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/bower/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository9(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Bower group repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/bower/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository10(body, callback)</td>
    <td style="padding:15px">Create Bower hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/bower/hosted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository10(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Bower hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/bower/hosted/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository11(body, callback)</td>
    <td style="padding:15px">Create Bower proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/bower/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository11(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Bower proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/bower/proxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository12(body, callback)</td>
    <td style="padding:15px">Create Docker group repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/docker/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository12(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Docker group repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/docker/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository13(body, callback)</td>
    <td style="padding:15px">Create Docker hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/docker/hosted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository13(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Docker hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/docker/hosted/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository14(body, callback)</td>
    <td style="padding:15px">Create docker proxy repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/docker/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository14(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Docker group repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/docker/proxy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository15(body, callback)</td>
    <td style="padding:15px">Create Git LFS hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/gitlfs/hosted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository15(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Git LFS hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/gitlfs/hosted/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository16(body, callback)</td>
    <td style="padding:15px">Create Yum hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/yum/hosted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository16(body, repositoryName, callback)</td>
    <td style="padding:15px">Update Yum hosted repository</td>
    <td style="padding:15px">{base_path}/{version}/beta/repositories/yum/hosted/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssets(continuationToken, repository, callback)</td>
    <td style="padding:15px">List assets</td>
    <td style="padding:15px">{base_path}/{version}/v1/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetById(id, callback)</td>
    <td style="padding:15px">Get a single asset</td>
    <td style="padding:15px">{base_path}/{version}/v1/assets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAsset(id, callback)</td>
    <td style="padding:15px">Delete a single asset</td>
    <td style="padding:15px">{base_path}/{version}/v1/assets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComponentById(id, callback)</td>
    <td style="padding:15px">Get a single component</td>
    <td style="padding:15px">{base_path}/{version}/v1/components/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteComponent(id, callback)</td>
    <td style="padding:15px">Delete a single component</td>
    <td style="padding:15px">{base_path}/{version}/v1/components/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComponents(continuationToken, repository, callback)</td>
    <td style="padding:15px">List components</td>
    <td style="padding:15px">{base_path}/{version}/v1/components?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadComponent(repository, yumDirectory, yumAsset, yumAssetFilename, rAsset, rAssetPathId, pypiAsset, helmAsset, aptAsset, rubygemsAsset, rawDirectory, rawAsset1, rawAsset1Filename, rawAsset2, rawAsset2Filename, rawAsset3, rawAsset3Filename, npmAsset, nugetAsset, maven2GroupId, maven2ArtifactId, maven2Version, maven2GeneratePom, maven2Packaging, maven2Asset1, maven2Asset1Classifier, maven2Asset1Extension, maven2Asset2, maven2Asset2Classifier, maven2Asset2Extension, maven2Asset3, maven2Asset3Classifier, maven2Asset3Extension, callback)</td>
    <td style="padding:15px">Upload a single component</td>
    <td style="padding:15px">{base_path}/{version}/v1/components?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContentSelector(name, callback)</td>
    <td style="padding:15px">Get a content selector by id</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/content-selectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateContentSelector(name, body, callback)</td>
    <td style="padding:15px">Update a content selector</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/content-selectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteContentSelector(name, callback)</td>
    <td style="padding:15px">Delete a content selector</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/content-selectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContentSelectors(callback)</td>
    <td style="padding:15px">List Content Selectors</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/content-selectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createContentSelector(body, callback)</td>
    <td style="padding:15px">Create a new content selector</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/content-selectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositories1(callback)</td>
    <td style="padding:15px">List repositories</td>
    <td style="padding:15px">{base_path}/{version}/v1/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingRules(callback)</td>
    <td style="padding:15px">List routing rules</td>
    <td style="padding:15px">{base_path}/{version}/beta/routing-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRoutingRule(body, callback)</td>
    <td style="padding:15px">Create a single routing rule</td>
    <td style="padding:15px">{base_path}/{version}/beta/routing-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingRule(name, callback)</td>
    <td style="padding:15px">Get a single routing rule</td>
    <td style="padding:15px">{base_path}/{version}/beta/routing-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoutingRule(name, body, callback)</td>
    <td style="padding:15px">Update a single routing rule</td>
    <td style="padding:15px">{base_path}/{version}/beta/routing-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoutingRule(name, callback)</td>
    <td style="padding:15px">Delete a single routing rule</td>
    <td style="padding:15px">{base_path}/{version}/beta/routing-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAssets(continuationToken, sort = 'group', direction = 'asc', timeout, q, repository, format, group, name, version, md5, sha1, sha256, sha512, prerelease, dockerImageName, dockerImageTag, dockerLayerId, dockerContentDigest, mavenGroupId, mavenArtifactId, mavenBaseVersion, mavenExtension, mavenClassifier, npmScope, nugetId, nugetTags, p2PluginName, pypiClassifiers, pypiDescription, pypiKeywords, pypiSummary, rubygemsDescription, rubygemsPlatform, rubygemsSummary, yumArchitecture, callback)</td>
    <td style="padding:15px">Search assets</td>
    <td style="padding:15px">{base_path}/{version}/v1/search/assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAndDownloadAssets(sort = 'group', direction = 'asc', timeout, q, repository, format, group, name, version, md5, sha1, sha256, sha512, prerelease, dockerImageName, dockerImageTag, dockerLayerId, dockerContentDigest, mavenGroupId, mavenArtifactId, mavenBaseVersion, mavenExtension, mavenClassifier, npmScope, nugetId, nugetTags, p2PluginName, pypiClassifiers, pypiDescription, pypiKeywords, pypiSummary, rubygemsDescription, rubygemsPlatform, rubygemsSummary, yumArchitecture, callback)</td>
    <td style="padding:15px">Search and download asset</td>
    <td style="padding:15px">{base_path}/{version}/v1/search/assets/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search(continuationToken, sort = 'group', direction = 'asc', timeout, q, repository, format, group, name, version, md5, sha1, sha256, sha512, prerelease, dockerImageName, dockerImageTag, dockerLayerId, dockerContentDigest, mavenGroupId, mavenArtifactId, mavenBaseVersion, mavenExtension, mavenClassifier, npmScope, nugetId, nugetTags, p2PluginName, pypiClassifiers, pypiDescription, pypiKeywords, pypiSummary, rubygemsDescription, rubygemsPlatform, rubygemsSummary, yumArchitecture, callback)</td>
    <td style="padding:15px">Search components</td>
    <td style="padding:15px">{base_path}/{version}/v1/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get1(format, callback)</td>
    <td style="padding:15px">Get upload field requirements for the desired format</td>
    <td style="padding:15px">{base_path}/{version}/v1/formats/{pathv1}/upload-specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get2(callback)</td>
    <td style="padding:15px">Get upload field requirements for each supported format</td>
    <td style="padding:15px">{base_path}/{version}/v1/formats/upload-specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">read(name, callback)</td>
    <td style="padding:15px">Read stored script by name</td>
    <td style="padding:15px">{base_path}/{version}/v1/script/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">edit(name, body, callback)</td>
    <td style="padding:15px">Update stored script by name</td>
    <td style="padding:15px">{base_path}/{version}/v1/script/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete1(name, callback)</td>
    <td style="padding:15px">Delete stored script by name</td>
    <td style="padding:15px">{base_path}/{version}/v1/script/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">browse(callback)</td>
    <td style="padding:15px">List all stored scripts</td>
    <td style="padding:15px">{base_path}/{version}/v1/script?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">add(body, callback)</td>
    <td style="padding:15px">Add a new script</td>
    <td style="padding:15px">{base_path}/{version}/v1/script?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">run1(name, body, callback)</td>
    <td style="padding:15px">Run stored script by name</td>
    <td style="padding:15px">{base_path}/{version}/v1/script/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmailConfiguration(callback)</td>
    <td style="padding:15px">Retrieve the current email configuration</td>
    <td style="padding:15px">{base_path}/{version}/beta/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setEmailConfiguration(body, callback)</td>
    <td style="padding:15px">Set the current email configuration</td>
    <td style="padding:15px">{base_path}/{version}/beta/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEmailConfiguration(callback)</td>
    <td style="padding:15px">Disable and clear the email configuration</td>
    <td style="padding:15px">{base_path}/{version}/beta/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testEmailConfiguration(body, callback)</td>
    <td style="padding:15px">Send a test email to the email address provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/beta/email/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isAvailable(callback)</td>
    <td style="padding:15px">Health check endpoint that validates server can respond to read requests</td>
    <td style="padding:15px">{base_path}/{version}/v1/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemStatusChecks(callback)</td>
    <td style="padding:15px">Health check endpoint that returns the results of the system status checks</td>
    <td style="padding:15px">{base_path}/{version}/v1/status/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isWritable(callback)</td>
    <td style="padding:15px">Health check endpoint that validates server can respond to read and write requests</td>
    <td style="padding:15px">{base_path}/{version}/v1/status/writable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">supportzip(body, callback)</td>
    <td style="padding:15px">Creates and downloads a support zip</td>
    <td style="padding:15px">{base_path}/{version}/v1/support/supportzip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapServers(callback)</td>
    <td style="padding:15px">List LDAP servers</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ldap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLdapServer(body, callback)</td>
    <td style="padding:15px">Create LDAP server</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ldap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapServer(name, callback)</td>
    <td style="padding:15px">Get LDAP server</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLdapServer(name, body, callback)</td>
    <td style="padding:15px">Update LDAP server</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLdapServer(name, callback)</td>
    <td style="padding:15px">Delete LDAP server</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeOrder(body, callback)</td>
    <td style="padding:15px">Change LDAP server order</td>
    <td style="padding:15px">{base_path}/{version}/beta/security/ldap/change-order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseStatus(callback)</td>
    <td style="padding:15px">Get the current license status.</td>
    <td style="padding:15px">{base_path}/{version}/beta/system/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setLicense(body, callback)</td>
    <td style="padding:15px">Upload a new license file.</td>
    <td style="padding:15px">{base_path}/{version}/beta/system/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeLicense(callback)</td>
    <td style="padding:15px">Uninstall license if present.</td>
    <td style="padding:15px">{base_path}/{version}/beta/system/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfiguration(callback)</td>
    <td style="padding:15px">Get IQ Server configuration</td>
    <td style="padding:15px">{base_path}/{version}/beta/iq?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfiguration(body, callback)</td>
    <td style="padding:15px">Update IQ Server configuration</td>
    <td style="padding:15px">{base_path}/{version}/beta/iq?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyConnection(callback)</td>
    <td style="padding:15px">Verify IQ Server connection</td>
    <td style="padding:15px">{base_path}/{version}/beta/iq/verify-connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableIq(callback)</td>
    <td style="padding:15px">Enable IQ Server</td>
    <td style="padding:15px">{base_path}/{version}/beta/iq/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableIq(callback)</td>
    <td style="padding:15px">Disable IQ Server</td>
    <td style="padding:15px">{base_path}/{version}/beta/iq/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
